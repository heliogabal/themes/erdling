/*location.hash returns the anchor part of an URL as a string,
with hash (#) symbol included. */
//wait for page elements to load
//execute function only if the anchor exists in the URL address
window.onload = function() {if(location.hash){
    //remove # from the string
    var elId = location.hash.replace('#','');
    //locate the anchored element on the page by its ID property
    var scrollToEl = document.getElementById(elId);
    //scroll to the anchored element
    scrollToEl.scrollIntoView(true);
  }
}
